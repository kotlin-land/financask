package br.com.alura.financask.extension

import java.text.DateFormat
import java.text.SimpleDateFormat
import java.util.*

fun Calendar.getFormattedDate(): String {
    val dateFormat = SimpleDateFormat.getDateInstance(DateFormat.SHORT)
    return dateFormat.format(this.time)
}

fun Calendar.yesterday(): Calendar {
    this.add(Calendar.DAY_OF_MONTH, -1)
    return this
}