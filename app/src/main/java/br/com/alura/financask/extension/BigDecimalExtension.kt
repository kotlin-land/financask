package br.com.alura.financask.extension

import java.math.BigDecimal
import java.text.NumberFormat

fun BigDecimal.getFormattedCurreny(): String {
    val numberFormat = NumberFormat.getCurrencyInstance()
    return numberFormat.format(this)
}