package br.com.alura.financask.model

import java.math.BigDecimal
import java.util.*

class Transacao(
    val valor: BigDecimal,
    val tipo: TipoTransacao,
    val categoria: String = "Diversos",
    val data: Calendar = Calendar.getInstance()
)