package br.com.alura.financask.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v4.content.ContextCompat
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import br.com.alura.financask.R
import br.com.alura.financask.extension.getFormattedCurreny
import br.com.alura.financask.extension.getFormattedDate
import br.com.alura.financask.model.TipoTransacao
import br.com.alura.financask.model.Transacao
import kotlinx.android.synthetic.main.transacao_item.view.*

class ListaTransacoesAdapter(
    private val context: Context,
    private val transacoes: List<Transacao>
) : BaseAdapter() {

    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view = LayoutInflater.from(context)
            .inflate(R.layout.transacao_item, parent, false)

        val transacao = getItem(position)

        inicializarUi(view, transacao)

        return view
    }

    private fun inicializarUi(
        view: View,
        transacao: Transacao
    ) {
        defineComponenteValor(view, transacao)
        view.transacao_data.text = transacao.data.getFormattedDate()
        view.transacao_categoria.text = transacao.categoria
        view.transacao_icone.setBackgroundResource(drawableDoTipoDa(transacao))
    }

    private fun defineComponenteValor(
        view: View,
        transacao: Transacao
    ) {
        view.transacao_valor.text = transacao.valor.getFormattedCurreny()
        view.transacao_valor.setTextColor(corDoTipoDa(transacao))
    }

    private fun corDoTipoDa(transacao: Transacao): Int = when (transacao.tipo) {
        TipoTransacao.RECEITA -> ContextCompat.getColor(context, R.color.receita)
        TipoTransacao.DESPESA -> ContextCompat.getColor(context, R.color.despesa)
    }

    private fun drawableDoTipoDa(transacao: Transacao): Int = when (transacao.tipo) {
        TipoTransacao.RECEITA -> R.drawable.icone_transacao_item_receita
        TipoTransacao.DESPESA -> R.drawable.icone_transacao_item_despesa
    }

    override fun getItem(position: Int): Transacao = transacoes[position]

    override fun getItemId(position: Int): Long = 0

    override fun getCount(): Int = transacoes.size
}