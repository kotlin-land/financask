package br.com.alura.financask.ui

import br.com.alura.financask.extension.getFormattedCurreny
import br.com.alura.financask.model.TipoTransacao
import br.com.alura.financask.model.Transacao
import java.math.BigDecimal

class ResumoView(private val transacoes: List<Transacao>) {

    fun totalizarReceitas(): String {
        var totalReceita = BigDecimal.ZERO
        for (transacao in transacoes) {
            if (transacao.tipo == TipoTransacao.RECEITA) {
                totalReceita = totalReceita.plus(transacao.valor)
            }
        }
        return totalReceita.getFormattedCurreny()
    }

    fun totalizarDespesas(): String {
        var totalDespesa = BigDecimal.ZERO
        for (transacao in transacoes) {
            if (transacao.tipo == TipoTransacao.DESPESA) {
                totalDespesa = totalDespesa.plus(transacao.valor)
            }
        }
        return totalDespesa.getFormattedCurreny()
    }
}