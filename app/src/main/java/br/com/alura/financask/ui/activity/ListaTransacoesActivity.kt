package br.com.alura.financask.ui.activity

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import br.com.alura.financask.R
import br.com.alura.financask.extension.yesterday
import br.com.alura.financask.model.TipoTransacao
import br.com.alura.financask.model.Transacao
import br.com.alura.financask.ui.ResumoView
import br.com.alura.financask.ui.adapter.ListaTransacoesAdapter
import kotlinx.android.synthetic.main.activity_lista_transacoes.*
import kotlinx.android.synthetic.main.resumo_card.*
import java.math.BigDecimal
import java.util.*

class ListaTransacoesActivity : AppCompatActivity() {

    private val transacoes: List<Transacao> = listarTransacoes()
    private var resumoView: ResumoView =
        ResumoView(transacoes)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_lista_transacoes)
        inicializarUi()
    }

    private fun inicializarUi() {
        lista_transacoes_listview.adapter = ListaTransacoesAdapter(this, transacoes)
        resumo_card_receita.text = resumoView.totalizarReceitas()
        resumo_card_despesa.text = resumoView.totalizarDespesas()
    }

    private fun listarTransacoes(): List<Transacao> =
        listOf(
            Transacao(
                BigDecimal(8.6),
                TipoTransacao.DESPESA,
                "Lanche no Bieza",
                Calendar.getInstance().yesterday()
            ),
            Transacao(
                BigDecimal(115.28),
                TipoTransacao.DESPESA,
                "Supermercado Imperatriz de São João do Norte",
                Calendar.getInstance().yesterday()
            ),
            Transacao(
                BigDecimal(4629.17),
                TipoTransacao.RECEITA,
                "Salário Softplan"
            ),
            Transacao(
                valor = BigDecimal(6),
                tipo = TipoTransacao.DESPESA
            )
        )
}